<?php
$view = new view();
$view->name = 'products';
$view->description = 'Products catalog';
$view->tag = 'default';
$view->base_table = 'eck_catalog';
$view->human_name = 'Products';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'eck list catalog product entities';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
$handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '5, 10, 20, 40, 60, 100, 200';
$handler->display->display_options['style_plugin'] = 'fluid_grid';
$handler->display->display_options['style_options']['items_width'] = '180';
$handler->display->display_options['style_options']['advanced_layout'] = array(
  'align' => TRUE,
);
$handler->display->display_options['style_options']['items_alignment'] = 'center';
$handler->display->display_options['style_options']['box_shadow'] = '0';
$handler->display->display_options['style_options']['border_radius'] = '0';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Catalog: Photos */
$handler->display->display_options['fields']['photos']['id'] = 'photos';
$handler->display->display_options['fields']['photos']['table'] = 'field_data_photos';
$handler->display->display_options['fields']['photos']['field'] = 'photos';
$handler->display->display_options['fields']['photos']['label'] = '';
$handler->display->display_options['fields']['photos']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['photos']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['photos']['settings'] = array(
  'image_style' => 'large_thmbnail',
  'image_link' => 'content',
);
$handler->display->display_options['fields']['photos']['delta_limit'] = '1';
$handler->display->display_options['fields']['photos']['delta_offset'] = '0';
/* Field: Catalog: Name */
$handler->display->display_options['fields']['sku']['id'] = 'sku';
$handler->display->display_options['fields']['sku']['table'] = 'eck_catalog';
$handler->display->display_options['fields']['sku']['field'] = 'sku';
$handler->display->display_options['fields']['sku']['label'] = '';
$handler->display->display_options['fields']['sku']['exclude'] = TRUE;
$handler->display->display_options['fields']['sku']['element_label_colon'] = FALSE;
/* Field: Catalog: Link */
$handler->display->display_options['fields']['view_link']['id'] = 'view_link';
$handler->display->display_options['fields']['view_link']['table'] = 'eck_catalog';
$handler->display->display_options['fields']['view_link']['field'] = 'view_link';
$handler->display->display_options['fields']['view_link']['label'] = '';
$handler->display->display_options['fields']['view_link']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['view_link']['alter']['text'] = '[sku]';
$handler->display->display_options['fields']['view_link']['element_label_colon'] = FALSE;
/* Field: Catalog: Edit link */
$handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['table'] = 'eck_catalog';
$handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['label'] = '';
$handler->display->display_options['fields']['edit_link']['element_label_colon'] = FALSE;
/* Field: Catalog: Delete link */
$handler->display->display_options['fields']['delete_link']['id'] = 'delete_link';
$handler->display->display_options['fields']['delete_link']['table'] = 'eck_catalog';
$handler->display->display_options['fields']['delete_link']['field'] = 'delete_link';
$handler->display->display_options['fields']['delete_link']['label'] = '';
$handler->display->display_options['fields']['delete_link']['element_label_colon'] = FALSE;
/* Filter criterion: Catalog: SKU (sku) */
$handler->display->display_options['filters']['product_name_value']['id'] = 'sku_value';
$handler->display->display_options['filters']['product_name_value']['table'] = 'field_data_sku';
$handler->display->display_options['filters']['product_name_value']['field'] = 'sku_value';
$handler->display->display_options['filters']['product_name_value']['operator'] = 'starts';
$handler->display->display_options['filters']['product_name_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['product_name_value']['expose']['operator_id'] = 'sku_value_op';
$handler->display->display_options['filters']['product_name_value']['expose']['label'] = 'SKU';
$handler->display->display_options['filters']['product_name_value']['expose']['operator'] = 'sku_value_op';
$handler->display->display_options['filters']['product_name_value']['expose']['identifier'] = 'sku_value';
$handler->display->display_options['filters']['product_name_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
/* Filter criterion: Catalog: Name */
$handler->display->display_options['filters']['sku']['id'] = 'sku';
$handler->display->display_options['filters']['sku']['table'] = 'eck_catalog';
$handler->display->display_options['filters']['sku']['field'] = 'sku';
$handler->display->display_options['filters']['sku']['operator'] = 'contains';
$handler->display->display_options['filters']['sku']['exposed'] = TRUE;
$handler->display->display_options['filters']['sku']['expose']['operator_id'] = 'sku_op';
$handler->display->display_options['filters']['sku']['expose']['label'] = 'SKU';
$handler->display->display_options['filters']['sku']['expose']['operator'] = 'sku_op';
$handler->display->display_options['filters']['sku']['expose']['identifier'] = 'sku';
$handler->display->display_options['filters']['sku']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
?>
