<?php

$instance = array(
  'label' => 'Quantity',
  'widget' => array(
    'weight' => '2',
    'type' => 'number',
    'module' => 'number',
    'active' => 0,
    'settings' => array(),
  ),
  'settings' => array(
    'min' => '',
    'max' => '',
    'prefix' => '',
    'suffix' => '',
    'user_register_form' => FALSE,
  ),
  'display' => array(
    'default' => array(
      'label' => 'above',
      'type' => 'number_decimal',
      'settings' => array(
        'thousand_separator' => '',
        'decimal_separator' => '.',
        'scale' => 2,
        'prefix_suffix' => TRUE,
      ),
      'module' => 'number',
      'weight' => 1,
    ),
    'teaser' => array(
      'type' => 'hidden',
      'label' => 'above',
      'settings' => array(),
      'weight' => 0,
    ),
  ),
  'required' => 1,
  'description' => '',
  'default_value' => array(
    array(
      'value' => '1',
    ),
  ),
  'field_name' => 'field_quantity',
  'entity_type' => 'set',
  'bundle' => 'item',
  'deleted' => '0',
);
?>
