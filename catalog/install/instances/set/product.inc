<?php
$instance = array(
  'label' => 'Product',
  'widget' => array(
    'weight' => '1',
    'type' => 'entityreference_autocomplete',
    'module' => 'entityreference',
    'active' => 1,
    'settings' => array(
      'match_operator' => 'CONTAINS',
      'size' => '20',
      'path' => '',
    ),
  ),
  'settings' => array(
    'user_register_form' => FALSE,
  ),
  'display' => array(
    'default' => array(
      'label' => 'above',
      'type' => 'entityreference_label',
      'settings' => array(
        'link' => FALSE,
        'bypass_access' => FALSE,
      ),
      'module' => 'entityreference',
      'weight' => 0,
    ),
    'teaser' => array(
      'type' => 'hidden',
      'label' => 'above',
      'settings' => array(),
      'weight' => 0,
    ),
  ),
  'required' => 1,
  'description' => '',
  'default_value' => NULL,
  'field_name' => 'field_product',
  'entity_type' => 'set',
  'bundle' => 'item',
  'deleted' => '0',
);
?>
