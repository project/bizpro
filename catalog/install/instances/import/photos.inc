<?php
$instance = array(
  'label' => $t('Photos'),
  'widget' => array(
    'weight' => '3',
    'type' => 'image_image',
    'module' => 'image',
    'active' => 1,
    'settings' => array(
      'progress_indicator' => 'throbber',
      'preview_image_style' => 'thumbnail',
      'filefield_sources' => array(
        'filefield_sources' => array(
          'upload' => 'upload',
          'plupload' => 'plupload',
          'upload' => 0,
          'remote' => 0,
          'clipboard' => 0,
          'reference' => 0,
          'attach' => 0,
        ),
        'source_reference' => array(
          'autocomplete' => '0',
          'search_all_fields' => '0',
        ),
        'source_imce' => array(
          'imce_mode' => 0,
        ),
        'source_attach' => array(
          'path' => 'file_attach',
          'absolute' => '0',
          'attach_mode' => 'move',
        ),
      ),
    ),
  ),
  'settings' => array(
    'file_directory' => 'catalog',
    'file_extensions' => 'png gif jpg jpeg',
    'max_filesize' => '',
    'max_resolution' => '',
    'min_resolution' => '',
    'alt_field' => 0,
    'title_field' => 0,
    'default_image' => 0,
    'user_register_form' => FALSE,
  ),
  'display' => array(
    'default' => array(
      'label' => 'above',
      'type' => 'image',
      'settings' => array(
        'image_style' => '',
        'image_link' => '',
      ),
      'module' => 'image',
      'weight' => 1,
    ),
    'teaser' => array(
      'type' => 'hidden',
      'label' => 'above',
      'settings' => array(),
      'weight' => 0,
    ),
  ),
  'required' => 0,
  'description' => 'Upload all photos that are mentioned in the Excel file to import.',
  'field_name' => 'photos',
  'entity_type' => 'op',
  'bundle' => 'catalog_import',
  'deleted' => '0',
);
?>
