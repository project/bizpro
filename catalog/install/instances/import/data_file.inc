<?php
$instance = array(
  'label' => $t('Data File'),
  'widget' => array(
    'weight' => '1',
    'type' => 'file_generic',
    'module' => 'file',
    'active' => 1,
    'settings' => array(
      'progress_indicator' => 'throbber',
      'filefield_sources' => array(
        'filefield_sources' => array(
          'upload' => 'upload',
          'plupload' => 0,
          'remote' => 0,
          'clipboard' => 0,
          'reference' => 0,
          'attach' => 0,
        ),
        'source_reference' => array(
          'autocomplete' => '0',
          'search_all_fields' => '0',
        ),
        'source_imce' => array(
          'imce_mode' => 0,
        ),
        'source_attach' => array(
          'path' => 'file_attach',
          'absolute' => '0',
          'attach_mode' => 'move',
        ),
      ),
    ),
  ),
  'settings' => array(
    'file_directory' => 'catalog/import',
    'file_extensions' => 'xls xlsx',
    'max_filesize' => '',
    'description_field' => 0,
    'user_register_form' => FALSE,
  ),
  'display' => array(
    'default' => array(
      'label' => 'above',
      'type' => 'file_default',
      'settings' => array(),
      'module' => 'file',
      'weight' => 0,
    ),
    'teaser' => array(
      'type' => 'hidden',
      'label' => 'above',
      'settings' => array(),
      'weight' => 0,
    ),
  ),
  'required' => 1,
  'description' => '',
  'field_name' => 'field_data_file',
  'entity_type' => 'op',
  'bundle' => 'catalog_import',
  'deleted' => '0',
);
?>
