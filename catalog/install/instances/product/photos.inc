<?php
$instance = array(
  'label' => $t('Photos'),
  'cardinality' => FIELD_CARDINALITY_UNLIMITED,
  'type' => 'photos',
  'settings' => array(
    'progress_indicator' => 'throbber',
    'preview_image_style' => 'thumbnail',
    'filefield_sources' => array(
      'filefield_sources' => array(
        'upload' => 'upload',
        'plupload' => 'plupload',
        'remote' => 0,
        'clipboard' => 0,
        'reference' => 0,
        'attach' => 0,
      ),
      'source_reference' => array(
        'autocomplete' => '0',
        'search_all_fields' => '0',
      ),
      'source_imce' => array(
        'imce_mode' => 0,
      ),
      'source_attach' => array(
        'path' => 'file_attach',
        'absolute' => '0',
        'attach_mode' => 'move',
      ),
    ),
  ),

  'widget' => array(
      'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
      ),
  ),
  'display' => array(
      'default' => array(
          'label' => 'hidden',
          'type' => 'image',
          'settings' => array('image_style' => 'medium', 'image_link' => ''),
          'weight' => -1,
      ),
      'teaser' => array(
          'label' => 'hidden',
          'type' => 'image',
          'settings' => array('image_style' => 'thumbnail', 'image_link' => 'content'),
          'weight' => -1,
      ),
  ),
);
?>
