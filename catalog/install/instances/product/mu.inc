<?php
$instance = array(
  'label' => $t('Measurement Unit'),
  'cardinality' => 1,
  'widget' => array(
    'weight' => 3,
    'type' => 'options_select',
    'module' => 'options',
    'active' => 1,
    'settings' => array(),
  ),
  'settings' => array(
    'user_register_form' => FALSE,
  ),
  'display' => array(
    'default' => array(
      'label' => 'above',
      'type' => 'list_default',
      'settings' => array(),
      'module' => 'list',
      'weight' => 2,
    ),
    'teaser' => array(
      'type' => 'hidden',
      'label' => 'above',
      'settings' => array(),
      'weight' => 0,
    ),
  ),
  'required' => 1,
  'description' => '',
  'default_value' => array(
    array(
      'value' => 'unit',
    ),
  ),
  'field_name' => 'mu',
  'entity_type' => 'catalog',
  'bundle' => 'product',
  'deleted' => '0',
);
?>
