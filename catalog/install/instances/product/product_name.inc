<?php
$instance = array(
  'label' => $t('Product Name'),
  'cardinality' => 1,
  'required' => 1,
  'widget' => array(
    'type' => 'text_textfield',
    'module' => 'text',
    'active' => 1,
    'settings' => array(
      'size' => 45,
    ),
  ),
  'settings' => array(
    'text_processing' => 0,
  ),
);
?>
