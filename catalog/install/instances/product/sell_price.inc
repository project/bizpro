<?php
$instance = array(
  'label' => $t('Sell Price'),
  'cardinality' => 1,
  'widget' => array(
    'weight' => 2,
    'type' => 'text_textfield',
    'module' => 'text',
    'active' => 1,
    'settings' => array(
      'size' => '15',
    ),
  ),
  'settings' => array(
    'min' => '',
    'max' => '',
    'prefix' => '',
    'suffix' => '元',
    'user_register_form' => FALSE,
    'text_processing' => 0,
  ),
  'display' => array(
    'default' => array(
      'label' => 'above',
      'type' => 'number_decimal',
      'settings' => array(
        'thousand_separator' => '',
        'decimal_separator' => '.',
        'scale' => 2,
        'prefix_suffix' => TRUE,
      ),
      'module' => 'number',
      'weight' => 1,
    ),
    'teaser' => array(
      'type' => 'hidden',
      'label' => 'above',
      'settings' => array(),
      'weight' => 0,
    ),
  ),
  'required' => 0,
  'description' => '',
  'default_value' => NULL,
  'field_name' => 'sell_price',
  'entity_type' => 'catalog',
  'bundle' => 'product',
  'deleted' => '0',
);
?>
