<?php
$instance = array(
  'label' => $t('Set Items'),
  'widget' => array(
    'weight' => '1',
    'type' => 'inline_entity_form',
    'module' => 'inline_entity_form',
    'active' => 1,
    'settings' => array(
      'fields' => array(),
      'type_settings' => array(
        'allow_new' => 1,
        'allow_existing' => 0,
        'match_operator' => 'CONTAINS',
        'allow_clone' => 0,
        'delete_references' => 1,
        'override_labels' => 1,
        'label_singular' => 'item',
        'label_plural' => 'items',
      ),
    ),
  ),
  'settings' => array(
    'user_register_form' => FALSE,
  ),
  'display' => array(
    'default' => array(
      'label' => 'above',
      'type' => 'entityreference_label',
      'settings' => array(
        'link' => FALSE,
        'bypass_access' => FALSE,
      ),
      'module' => 'entityreference',
      'weight' => 0,
    ),
    'teaser' => array(
      'type' => 'hidden',
      'label' => 'above',
      'settings' => array(),
      'weight' => 0,
    ),
  ),
  'required' => 1,
  'description' => '',
  'field_name' => 'items',
  'entity_type' => 'catalog',
  'bundle' => 'set',
  'deleted' => '0',
);
?>
