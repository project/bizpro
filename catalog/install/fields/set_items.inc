<?php
$field = array(
  'label' => $t('Set Items'),  
  'cardinality' => FIELD_CARDINALITY_UNLIMITED,
  'type' => 'entityreference',
  'settings' => array(
    'target_type' => 'set',
    'handler' => 'base',
    'handler_settings' => array(
      'target_bundles' => array(
        'item' => 'item',
      ),
      'sort' => array(
        'type' => 'none',
      ),
      'behaviors' => array(
        'views-select-list' => array(
          'status' => 0,
        ),
      ),
    ),
  ),
  'module' => 'entityreference',
  'deleted' => 0,
  'indexes' => array(
    'target_id' => array('target_id'),
  ),
);
?>
