<?php
$field = array(
  'field_name' => 'name',
  'label' => $t('Product Name'),
  'cardinality' => 1,
  'type' => 'text',
  'module' => 'text',
  'length' => 255,
  'settings' => array(
    'max_length' => 255,
    'text_processing' => 0,
  ),
);
?>
