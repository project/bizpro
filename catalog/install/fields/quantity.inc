<?php
$field = array(
  'label' => $t('Quantity'),  
  'translatable' => '0',
  'entity_types' => array(),
  'settings' => array(
    'target_type' => 'catalog',
    'handler' => 'base',
    'handler_settings' => array(
      'target_bundles' => array(
        'product' => 'product',
      ),
      'sort' => array(
        'type' => 'none',
      ),
      'behaviors' => array(
        'views-select-list' => array(
          'status' => 0,
        ),
      ),
    ),
  ),
  'foreign keys' => array(
    'eck_catalog' => array(
      'table' => 'eck_catalog',
      'columns' => array(
        'target_id' => 'id',
      ),
    ),
  ),
  'indexes' => array(
    'target_id' => array(
      'target_id',
    ),
  ),
  'field_name' => 'field_product',
  'type' => 'entityreference',
  'module' => 'entityreference',
  'active' => '1',
  'locked' => '0',
  'cardinality' => '1',
  'deleted' => '0',
);
?>
