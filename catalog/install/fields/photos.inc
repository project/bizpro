<?php
$field = array(
  'label' => $t('Photos'),
  'cardinality' => FIELD_CARDINALITY_UNLIMITED,
  'type' => 'image',
  'settings' => array(
    'default_image' => 0,
    'uri_scheme' => 'public',
  ),
);
?>
