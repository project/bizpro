<?php
$field = array(
  'label' => $t('Sell Price'),
  'translatable' => '0',
  'entity_types' => array(),
  'settings' => array(
    'precision' => '10',
    'scale' => '2',
    'decimal_separator' => '.',
    'max_length' => 10,
    'text_processing' => 0,
  ),
  'type' => 'number_decimal',
  'module' => 'number',
  'active' => '1',
  'locked' => '0',
  'cardinality' => '1',
  'deleted' => '0',
);
?>
