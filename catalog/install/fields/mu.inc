<?php
$field = array(
  'label' => $t('Measurement Unit'),
  'translatable' => '0',
  'entity_types' => array(),
  'settings' => array(
    'allowed_values' => array(
      'unit' => 'unit',
      'pair' => 'pair',
      'kg' => 'kg',
      'l' => 'litter',
      'm' => 'm',
      'sqm' => 'sq.m',
      'cbm' => 'cb.m',
    ),
    'allowed_values_function' => '',
  ),
  'indexes' => array(
    'value' => array(
      'value',
    ),
  ),
  'field_name' => 'field_mu',
  'type' => 'list_text',
  'module' => 'list',
  'active' => '1',
  'locked' => '0',
  'cardinality' => '1',
  'deleted' => '0',
);
?>
