<?php
$field = array(
  'label' => $t('Data File'),  
  'translatable' => '0',
  'settings' => array(
    'display_field' => 0,
    'display_default' => 0,
    'uri_scheme' => 'public',
  ),
  'indexes' => array(
    'fid' => array(
      'fid',
    ),
  ),
  'field_name' => 'field_data_file',
  'type' => 'file',
  'module' => 'file',
  'active' => '1',
  'locked' => '0',
  'cardinality' => '1',
  'deleted' => '0',
);
?>
