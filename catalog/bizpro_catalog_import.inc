<?php

/**
 * Function bizpro_catalog_import_column_keys().
 *
 * Provides an assosiative array of column names and keys that may hold data to import.
 */
function bizpro_catalog_import_column_keys() {
  $keys = array(
    'SKU' => 'sku',
    'Code' => 'sku',
    'ID' => 'sku',
    'Name'=> 'product_name',
    'Product Name'=> 'product_name',
    'Commodity'=> 'product_name',
    'Title'=> 'product_name',
    'Sell Price'=> 'sell_price',
    'Image'=> 'photos',
    'Photo'=> 'photos',
  );

  foreach ($keys as $k => $v) {
    $keys[t($k)] = $v;
  }
  return $keys;
}

/**
 * Function bizpro_catalog_import_get_sheet_cols().
 *
 * Searches the given row for suitable to import column names
 *
 * @return Array of with 2 keys: cols and header
 * cols - found columns
 * header - header as to be used in tables.
 */
function bizpro_catalog_import_get_sheet_cols($oSheet, $colNr) {
  
  //search column names for titles:
  $done = false;
  $col = 0;
  $cols = $header = array();
  $keys = bizpro_catalog_import_column_keys();
  while(!$done) {
    $oCel = $oSheet->getCellByColumnAndRow($col, $colNr);
    $val = trim($oCel->getValue());
    if (!$val) {
      $done = TRUE;
    }
    else {
      if (isset($keys[$val])) {
        $cols[$keys[$val]] = $col;
        $header[$keys[$val]] = $val;
      }
    }
    $col++;
  }
  return array('cols' => $cols, 'header' => $header);
}

/**
 * Function bizpro_catalog_import_process().
 *
 * $entity - Catalog Importy type entity containing excel file and photots
 */
function bizpro_catalog_import_process($entity, $settings = array()) {
  
  $batch = array(
    'operations' => array(),
    'finished' => 'bizpro_catalog_import_finished',
    'title' => 'Import Products Catalog',
    'init_message' => 'Starting Import Process',
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Products Import has encountered an error.'),
    'file' => drupal_get_path('module', 'bizpro_catalog') . '/bizpro_catalog_import.inc',
  );

  $uri = $entity->field_data_file['und'][0]['uri'];
  /*
  $xlsFile = file_load($entity->field_data_file['und'][0]['fid']);

  $lotID = current(explode('_', $xlsFile->filename));
  $ext = end(explode('.', $xlsFile->filename));
   */
  require_once 'sites/all/libraries/PHPExcel/Classes/PHPExcel.php';
  require_once 'sites/all/libraries/PHPExcel/Classes/PHPExcel/IOFactory.php';

  if (!file_exists($uri)) {
    return array(
      '#markup' => "Cannot find file $uri",
    );
  }
  $oPHPExcel = PHPExcel_IOFactory::load(drupal_realpath($uri));
  $oSheet = $oPHPExcel->getActiveSheet();

  $keys = bizpro_catalog_import_column_keys();

  $c = bizpro_catalog_import_get_sheet_cols($oSheet, 1);
  $cols = $c['cols'];
  $header = $c['header'];

  $rows = array();
  $skuok = TRUE;
  $rowNr = 2;
  while($skuok) {
    $row = array();
    foreach ($cols as $ck=>$cv) {
      $row[$ck] = $oSheet->getCellByColumnAndRow($cv, $rowNr)->getValue();
    }
    $skuok = isset($row['sku']);
    if ($skuok) {
      $rows[] = $row;
    }
    $rowNr++;
  }

  $chunks = array_chunk($rows, 10);

  foreach ($chunks as $nr => $chunk) {
    $batch['operations'][$nr] = array('bizpro_catalog_import_rows', array($chunk, $nr));
  }

  batch_set($batch);
  batch_process(current_path());
}

/**
 * Function bizpro_catalog_import_rows().
 */
function bizpro_catalog_import_rows($rows, $operation_details, &$context) {
  $context['message'] = t('Importing batch @nr', array('@nr' => $operation_details));
  $context['results'][] = t('Imported @nr recordsa', array('@nr' => $operation_details * 10));
}

/**
 * Function bizpro_catalog_import_finished().
 */
function bizpro_catalog_import_finished($success, $results, $operations) {
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of nodes we processed...
    //drupal_set_message(t('@count results processed in @requests HTTP requests.', array('@count' => count($results), '@requests' => _batch_example_get_http_requests())));
    drupal_set_message(t('The final result was "%final"', array('%final' => end($results))));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(
      t('An error occurred while processing @operation with arguments : @args',
        array(
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        )
      ),
      'error'
    );
  }
}
?>
