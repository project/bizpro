<?php

/**
 * Leafbit Dev - development tools enabler.
 */

/**
 * Invoke hook_drush_command().
 */
function bizpro_drush_command() {
  $items['bizpro-uninstall-all'] = array(
    'description' => 'Uninstalls asll Business Pro Modules',
    'aliases' => array('bizua'),
  );
  $items['bizpro-install-all'] = array(
    'description' => 'Installs Business Pro modules',
    'aliases' => array('bizi'),
  );
  return $items;
}

/**
 * Function drush_leafbitdev_export_field().
 */
function drush_bizpro_install_all() {
  $modules = drush_bizpro_modules_to_process();
  module_enable($modules);
  echo "Enabled modules: " . implode(", ", $modules) . "\n";
}

/**
 * Function drush_leafbitdev_export_field().
 */
function drush_bizpro_uninstall_all() {
  $modules = drush_bizpro_modules_to_process();
  $toprocess = array();
  foreach($modules as $m) {
    if (module_exists($m)){
      $toprocess[] = $m;
    }
  }
  if ($toprocess) {
    module_disable($toprocess);
    drupal_uninstall_modules($toprocess);
  }
  echo "Disabled and uninstalled  modules: " . implode(", ", $toprocess) . "\n";
}
/**
 * Function bizpro.drush_modules_to_process().
 */
function drush_bizpro_modules_to_process($toinstall = FALSE) {
  $rz = array(
    'product',
    'catalog',
    'raw_material',
    'document',
    'import',
    'sale',
  );

  if ($toinstall) {
    $rz = array(
      'product',
      'catalog',
    ); 
  }

  foreach ($rz as $k=>$v) {
    $rz[$k] = 'bizpro_' . $v;
  }
  return $rz;
}
