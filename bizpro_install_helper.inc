<?php

class BizProInstallHelper {

  public $entity_type = FALSE;
  public $module = FALSE;

  /**
   * Checks if module variable is set:
   */
  public function hasModule($module = FALSE) {
    if ($module) {
      $this->module = $module;
    }
    return $this->module;
  }
  /**
   * Function to create entity (ECK)
   */
  public function CreateEntity($name, $label) {
    $entity_type = new EntityType();
    $entity_type->name = $name;
    $entity_type->label = $label;
    $entity_type->save();
    $this->entity_type = $name;
    return $entity_type;
  }

  public function hasEntityType($entity_type=FALSE) {
    if ($entity_type) {
      $this->entity_type = $entity_type;
    }

    if (!$this->entity_type) {
      drupal_set_message("Entity type is required for Bundle creation process", 'error');
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Helps to create a bundle
   */
  public function CreateBundle($name, $label, $entity_type=FALSE) {

    if (!$this->hasEntityType($entity_type)) return;

    $bundle = new Bundle();
    $bundle->name = $name;
    $bundle->label = $label;
    $bundle->entity_type = $this->entity_type;
    $bundle->save();

    return $bundle;
  }

  /**
   * Creates field instances
   */
  public function CreateInstances($instances, $bundle, $entity_type=FALSE) {
    
    if (!$this->hasEntityType($entity_type)) return;
    foreach ($instances as $field_name => $instance) {
      $instance['field_name'] = $field_name;
      $instance['entity_type'] = $this->entity_type;
      $instance['bundle'] = $bundle;
      field_create_instance($instance);
    }
  }

  /**
   * Create Fields
   */
  public function CreateFields($fields) {
    foreach ($fields as $field_name => $field) {
      $field['field_name'] = $field_name;
      field_create_field($field);
    }
  }

  /**
   * Get Predefined instance
   */
  public function getInstanceDefinition($key) {
    $path = drupal_get_path('module', 'bizpro') . '/def/' . $key . '.inc';
    if (file_exists($path)) {
      include($path);
      if (isset($instance)) return $instance;
    }
    return Array();
  }

  public function DeleteEntityTypes($types){
    if (!is_array($types)) $types = array($types);
    foreach ($types as $type) {
      $t = EntityType::LoadByName($type);
      if ($t) {
        $t->delete();
      }
    }
  }

  /**
   * Load fields and/or instances definition
   */
  public function loadFields($keys = FALSE, $subpath = 'fields', $module = FALSE, $returnKeys = FALSE) {
    if (!$this->hasModule($module)) return FALSE;
    $path = drupal_get_path('module', $this->module) . '/install/' . $subpath;
    $path = realpath($path);
    if (!$path) {
      drupal_set_message("Failed to load fields from path $path", "error");
      return array();
    }

    $files = array();

    $t = get_t();

    if ($keys) {
      if (!is_array($keys)) {
        $keys = array($keys);
      }
      foreach ($keys as $key) {
        $files[] = $path . '/' . $key . '.inc';
      }
    }
    else {
      $files = glob($path . '/*.inc');
    }
  
    $fields = array();
    $instances = array();
    if ($files) {
      foreach($files as $f) {
        $field = $instance = FALSE;
        $key = basename($f, ".inc");
        if ($returnKeys) {
          $fields[] = $key;
          continue;
        }
        include($f);
        if ($field) {
          $fields[$key] = $field;
        }
        elseif($instance) {
          $instances[$key] = $instance;
        }
      }
    }
    if ($fields && $instances) {
      return array('fields' => $fields, 'instances' => $instances);
    }
    elseif($fields) {
      return $fields;
    }
    return $instances;
  }

  /**
   * Install all module related views.
   */
  public function installViews($module = FALSE) {
    if (!$this->hasModule($module)) return FALSE;
    $path = drupal_get_path('module', $this->module) . '/install/views';
    $path = realpath($path);
    if (!$path) return FALSE;
    $files = glob($path . '/*.view.inc');
    foreach ($files as $f) {
      $view = FALSE;
      include($f);
      if ($view) {
        $view->save();
      }
    }
  }


  /**
   * Uninstall all module related views.
   */
  public function uninstallViews($module = FALSE) {
    if (!$this->hasModule($module)) return FALSE;
    $path = drupal_get_path('module', $this->module) . '/install/views';
    $path = realpath($path);
    if (!$path) return FALSE;
    $files = glob($path . '/*.view.inc');
    foreach ($files as $f) {
      $view = FALSE;
      include($f);
      if ($view && $v = views_get_view($view->name)) {
        views_delete_view($v);
      }
    }
  }
}
